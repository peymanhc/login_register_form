import React from 'react'
import { useForm } from 'react-hook-form'

const Loginbox = (props) => {
    const { register, handleSubmit } = useForm();

    const onSubmit = data => console.log(data)

    return (
        <div className={`box-form login ${props.class}`}>
            <div className='box-login-tab'></div>
            <div className='box-login-title'>
                <h5 style={{ marginLeft: "3em", marginTop: "17px" }} >LOGIN</h5>
            </div>
            <div className='box-login'>
                <div className='fieldset-body'>
                    <button onClick={props.click} className='b b-form' title='Mais Informações'>Register</button>
                    <form onSubmit={handleSubmit(onSubmit)}>
                    <p className='field'>
                        <label>E-MAIL</label>
                        <input ref={register} name="email" type='email' />
                    </p>
                    <p className='field'>
                        <label>PASSWORD</label>
                        <input ref={register} name="password" type='password' />
                    </p>
                    <input type='submit' value='Login' />
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Loginbox