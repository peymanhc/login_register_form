import React from 'react'
import { useForm } from 'react-hook-form'

const Register = (props) => {
    const { register, handleSubmit } = useForm();
    const OnSubmit = data => console.log(data)

    return (
        <div className={` ${props.class}`}>
            <div className='box-login-tab'></div>
            <div className='box-login-title'>
                <h5 style={{ marginLeft: "3em", marginTop: "17px" }} >Register</h5>
            </div>
            <div className='box-login'>
                <div className='fieldset-body'>
                    <button onClick={props.click} className='b b-form'>Login</button>

                    <form onSubmit={handleSubmit(OnSubmit)} >
                        <p className='field'>
                            <label>Name</label>
                            <input ref={register} name="name" type='text' />
                        </p>
                        <p className='field'>
                            <label>E-MAIL</label>
                            <input ref={register} name="email" type='email' />
                        </p>
                        <p className='field'>
                            <label>PASSWORD</label>
                            <input ref={register} name="password" type='password' title='Password' />
                        </p>
                        <input type='submit' value='Register' />
                    </form>
                </div>
            </div>
        </div>
    )
}

export default Register